package com.zuitt.example;

import java.util.Scanner;

public class ControlStructures {
    public static void main(String[] args){
        // Operators in Java
        // Arithmetic: PEMDAS
        // Comparison: >, <, >=, <=, ==, !=
        // Logical: &&, ||, !
        // Assignment: =

        // Conditional Structures in Java
        // Statements that allow us to manipulate the flow depending  on the evaluation of the condition
        // Syntax: if(condtion){}
        int num1 =  15;
        if(num1 % 5== 0){
            System.out.println(num1 + " is divisible by 5");
        }

        // else statement will allow us to run a task or code if the conditions fails or hava a falsy values.
        num1 = 36;
        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }

         // Mini Activity

        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number:");
//
//        int num = numberScanner.nextInt();
//
//         if(num % 2 == 0)
//             System.out.println(num + " is even");
//         else
//             System.out.println(num + " is odd");

         // Short-circuiting
        // a short circuit happens because the result is clear even before the complete evaluation of the expression, and the result is returned
        int x = 15;
        int y = 0;
        if(y == 0 || x == 15 || y !=0) System.out.println(true);

        // ternary operator
        int num2 = -1;
        String result = (num2 > 0) ? Boolean.toString(true):Boolean.toString(false);
        System.out.println(result);

        int num3 = 14;
        Boolean result2 = (num3 > 0) ? true :false;
        System.out.println(result2);

        // Switch Cases
        // control flow structures that allow one code block to be run out of manu other code block
        // this is often used when the input is predictable

        System.out.println("Enter a number from 1-4 to SM Males in one of the four directions");
        int directionValue = numberScanner.nextInt();

        switch(directionValue){
            case 1:
                System.out.println("SM North Edsa");
                break;
            case 2:
                System.out.println("SM South Mall");
                break;
            case 3:
                System.out.println("SM City Taytay");
                break;
            case 4:
                System.out.println("SM Manila");
                break;
            default:
                System.out.println("Out of Range");
        }

    }
}

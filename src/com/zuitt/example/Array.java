package com.zuitt.example;

import java.util.*;

public class Array {
    public static void main(String[] args){
       /*
         Arrays in Java

         In Java, arrays are significantly rigid, even before filling in the arrays we should already identify the data type and the size of the array.
         Syntax:
         dataType[] Identifier = new dataType[numberOfElements]
         dataType[] Identifier = {elementA, elementB, elementC, ...}
    */

        String[] newArr = new String[3];
        // toString() is used to show the values of the array as a string in terminal
        System.out.println(Arrays.toString(newArr));
        // display the memory address of the array
        System.out.println(newArr);
        newArr[0] = "Clark";
        System.out.println(Arrays.toString(newArr));
        newArr[1] ="Bruce";
        newArr[2] ="Lois";
        // newArr[3] ="Barry"; -> out of bounds from the number of elements
        // newArr[3] = 25; -> wrong provided data type
        System.out.println(Arrays.toString(newArr));

        String[] arrSample = {"Tony", "Steeve", "Thor"};
        System.out.println(Arrays.toString(arrSample));
        // arrSample[3] = "Peter"; -> out of bounds from the number of elements
        System.out.println(Arrays.toString(arrSample));

        Integer[] intArr = new Integer[5];
        intArr[0] = 54;
        intArr[1] = 23;
        intArr[2] = 25;
        intArr[3] = 30;
        intArr[4] = 62;
        System.out.println("Initial order of the intArr: " + Arrays.toString(intArr));

        Arrays.sort(intArr);
        System.out.println("Order of items after sort: "+ Arrays.toString(intArr));

        // Arrays.sort(originalArr, fromIndex, endIndex);
        Arrays.sort(intArr, Collections.reverseOrder());
        System.out.println("Order of items after sort: "+ Arrays.toString(intArr));

        //ArrayList
        /*
            Array List are resizable collections/ arrays that function similarly to how arrays work in JS
            Syntax: ArrayList<dataType> identifier = new ArrayList<>();
        */
        ArrayList<String> students = new ArrayList<>();
        // ArrayList Methods
        // arrayListName.add(itemToAdd) - adds elements in our list
        students.add("Paul");
        students.add("John");
        System.out.println(students);

        // arrayListName.get(index) - retrieve items from the array list using its index.
        System.out.println(students.get(1));

        // arrayListName.set(index, value) - update an item by its index
        students.set(0, "George");
        System.out.println(students);

        // arrayListName.remove(index) - removes an item by its index
        students.remove(1);
        System.out.println(students);

        students.add ("Ringo");

        // arrayList.clear(); - clears out items in the array list.
        students.clear();
        System.out.println(students);

        // arrayListName.size(); -> gets the length of our array list
        students.add("Wanda");
        students.add("Captain America");
        students.add("Wasp");
        System.out.println(students.size());

        // ArrayList with initialize values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Bill Gates", "Elon Musk", "Jeff Bezos"));
        System.out.println(employees);
        employees.add("Lucio Tan");
        System.out.println(employees);
        // add(index, elementToBeAdded)
        employees.add(2, "Henry Sy");
        System.out.println(employees);

        // Hashmaps
        // most objects in Java are defined and are instantiations of classes that contain a proper set of properties and methods. There might be use cases where it is not appropriate or you may simply want to store a collection of data in key-value pairs.
        // in Java "keys" also referred as "fields"
        // Syntax: HashMaps<fieldDataType, valueDataType> Identifier = new HshMaps();

        HashMap<String, String> userRoles = new HashMap<>();
        // add new field and value in the hashmap
        // hashMapName.put(<field, value>);
        userRoles.put("Anna", "Admin");
        userRoles.put("Alice", "Mage");
        System.out.println(userRoles);
        userRoles.put("Alice", "Marksman");
        System.out.println(userRoles);
        userRoles.put("Dennis", "Tank");
        System.out.println(userRoles);

        // retrieve values by fields
        // hashMapName.get("field");
        System.out.println(userRoles.get("Alice"));
        System.out.println(userRoles.get("Anna ")); // wtih space - will result to null
        System.out.println(userRoles.get("dennis")); // case-sensetive - will result to null
        System.out.println(userRoles.get("Emman")); // non-existing field - will result to null

        // removes an element / field-value
        // hashMapsName.remove("field")
        userRoles.remove("Anna");
        System.out.println(userRoles);

        // retrieve hashMap keys
        //hashMapName.keyset();
        System.out.println(userRoles.keySet());
    }
}

package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        String[] fruits = {"apple", "avocado", "banana", "kiwi", "orange"};
        System.out.println("Fruits in stock: " + Arrays.toString(fruits));

        Scanner myFruit = new Scanner(System.in);
        System.out.println("Which fruit would you like to get the index of? ");

        String fruit = myFruit.nextLine();
        System.out.println("The index of "+ fruit+ " is "+ Arrays.binarySearch(fruits,fruit));

        ArrayList<String> friends = new ArrayList<>();
        friends.add("Shane");
        friends.add("Carlo");
        friends.add("Daniel");
        friends.add("Jeff");
        System.out.println("My friends are:" + friends);

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consist of: "+ inventory);
    }
}
